DFT hands-on Oct 2021
=====================

A session where we briefly discussed how to compute the band structure of simple crystals with Quantum Espresso.

Input files:
- `scf.in`: an input for a self-consistent density calculation of FCC copper. Run as
  ```bash
  pw.x < scf.in
  ```
- `bands.in`: an input for bands calculation of FCC copper. Run as
  ```bash
  pw.x < bands.in
  ```
- `Cu.pbe-dn-rrkjus_psl.1.0.0.UPF`: copper pseudopotential used

![bands](bands.png)

Resources
---------

Quantum espresso home: https://www.quantum-espresso.org/

Parameters: https://www.quantum-espresso.org/Doc/INPUT_PW.html

Pseudopotentials: https://www.quantum-espresso.org/pseudopotentials

My code to parse bands and more: https://github.com/pulkin/dfttools (run as `dft-plot-bands bands.out -o bands.png` after installing)

